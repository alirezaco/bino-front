import "src/globals.css";
import "swiper/css"
import "swiper/css"
import "swiper/swiper-bundle.min.css";
import "swiper/swiper.min.css";
import "swiper/css/pagination";

import type { AppProps } from "next/app";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import RootLayout from "@/components/common/Layout";
// import { Provider } from "react-redux";
// import { PersistGate } from "redux-persist/integration/react";
// import { persistedStore, store } from "src/store";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      {/* <Provider store={store}>
        <PersistGate loading={null} persistor={persistedStore}> */}
      <RootLayout>
        <Component {...pageProps} />
        <ToastContainer
          draggable
          autoClose={4000}
          hideProgressBar
          pauseOnHover={false}
          position="bottom-left"
        />
      </RootLayout>
      {/* </PersistGate>
      </Provider> */}
    </>
  );
}
