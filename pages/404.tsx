import React from 'react'

const Error = () => {
  return (
    <div className='flex items-center justify-center'>صفحه مورد نظر پیدا نشد</div>
  )
}

export default Error