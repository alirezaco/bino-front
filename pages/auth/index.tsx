"use client";
import axios from "axios";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import tw from "tailwind-styled-components";
import { TailwindInterface } from "tailwind-styled-components/dist/tailwind";

const Auth = () => {
  const [isSelected, setIsSelected] = useState(false);

  useEffect(() => {
    const getSomething = async() =>{
      const res  = await axios.get("/api/hello")
      console.log(res.data);

    }
    getSomething()
  }, [])
  
  const str = `fle`;
  return (
    <div className="">
      use these links to g{isSelected && "o sign up or login page"}
      <Link href="auth/login">hello</Link>
      <Component $darkMode={isSelected}>hello</Component>
      <button onClick={() => setIsSelected(!isSelected)}>click</button>
    </div>
  );
};

export default Auth;

const darkModeHandler= (): any => {
  return (p: any) => (p.$darkMode ? "bg-slate-300 text-emerald-600" : "bg-black");
};

const Component = tw.div<{ $darkMode: boolean }>`
flex
bg-slate-800
${darkModeHandler()}
`;
