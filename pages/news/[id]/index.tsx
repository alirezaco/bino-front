import Modal from "@/components/common/Modal";
import { useRouter } from "next/router";
import { useState } from "react";
import { toast } from "react-toastify";

const News = ({ params }: any) => {
  const { query } = useRouter();
  const [isModalOpen, setIsModalOpen] = useState(true);
  return (
    <div>
      {query.id}
      <Modal isModalOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
        <div className="min-h-[200px] min-w-[200px] bg-white" onClick={()=>toast.error("ssss")}>
          hello
        </div>
      </Modal>
    </div>
  );
};

export default News;
