module.exports = {
    singleQuote: true,
    semi: false,
    bracketSpacing: false,
    endOfLine: "lf",
    bracketSameLine: false,
    printWidth: 100,
    quoteProps: "as-needed",
    requirePragma: false,
    tabWidth: 2,
    trailingComma: "all",
    useTabs: false,
    plugins: [require('prettier-plugin-tailwindcss')],
  }