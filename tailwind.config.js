/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        gray: {
          light: "#E4E4E4",
        },
      },
      backgroundImage: {
        hero_poster: "url(/image/landing/Poster-hero.png)",
        bg_event: "url(/image/landing/bg-event.png)",
      },
    },
  },
  plugins: [],
};
