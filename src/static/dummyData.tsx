import tutorial1 from "public/image/landing/Rectangle19.png";
import tutorial2 from "public/image/landing/Rectangle31.png";
import tutorial3 from "public/image/landing/Rectangle33.png";
import tutorial4 from "public/image/landing/Rectangle35.png";
import tutorial5 from "public/image/landing/Rectangle37.png";
import news1 from "public/image/landing/Rectangle43.png";
import news2 from "public/image/landing/Rectangle44.png";
import news3 from "public/image/landing/Rectangle45.png";

export const TUTORIALS_TEST_DEFAULT_LIST = [
  {
    img: tutorial1,
    title: "دوره آموزش NFT در بینوهاب",
    detail:
      "در این آموزش در رابطه با تنوع محصولات آن در این پلتفرم NFT خواهیم پرداخت",
    price: "2.350.000 تومان",
    id: "tutorial1",
  },
  {
    img: tutorial2,
    title: "دوره آموزش NFT در بینوهاب",
    detail:
      "در این آموزش در رابطه با تنوع محصولات آن در این پلتفرم NFT خواهیم پرداخت",
    price: "2.350.000 تومان",
    id: "tutorial2",
  },
  {
    img: tutorial3,
    title: "دوره آموزش NFT در بینوهاب",
    detail:
      "در این آموزش در رابطه با تنوع محصولات آن در این پلتفرم NFT خواهیم پرداخت",
    price: "2.350.000 تومان",
    id: "tutorial3",
  },
  {
    img: tutorial4,
    title: "دوره آموزش NFT در بینوهاب",
    detail:
      "در این آموزش در رابطه با تنوع محصولات آن در این پلتفرم NFT خواهیم پرداخت",
    price: "2.350.000 تومان",
    id: "tutorial4",
  },
  {
    img: tutorial5,
    title: "دوره آموزش NFT در بینوهاب",
    detail:
      "در این آموزش در رابطه با تنوع محصولات آن در این پلتفرم NFT خواهیم پرداخت",
    price: "رایگان",
    id: "tutorial5",
  },
  ,
];

export const NEWS_TEST_DEFAULT_LIST = [
  {
    img: news1,
    id: "news1",
    title:
      "دوره آموزش NFT در بینوهاب در سایت بینوهاب با اخذ نمایندگی در تمام دنیا",
    detail:
      "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم‌افزارها شناخت بیشتری را برای طراحان...",
    date: new Date(),
    creator: "admin",
    tags: ["بینوهاب", "NFT", "تکنو"],
  },
  {
    img: news2,
    id: "news2",
    title:
      "دوره آموزش NFT در بینوهاب در سایت بینوهاب با اخذ نمایندگی در تمام دنیا",
    detail:
      "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم‌افزارها شناخت بیشتری را برای طراحان...",
    date: new Date(),
    creator: "admin",
    tags: ["بینوهاب", "NFT", "تکنو"],
  },
  {
    img: news3,
    id: "news3",
    title:
      "دوره آموزش NFT در بینوهاب در سایت بینوهاب با اخذ نمایندگی در تمام دنیا",
    detail:
      "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم‌افزارها شناخت بیشتری را برای طراحان...",
    date: new Date(),
    creator: "admin",
    tags: ["بینوهاب", "NFT", "تکنو"],
  },
];
