import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";

import Hexagon from "@/components/Hexagon";
import { FiArrowLeft, FiChevronLeft } from "react-icons/fi";
import logo1 from "public/image/landing/01.png";
import logo2 from "public/image/landing/02.png";
import logo3 from "public/image/landing/03.png";
import event1 from "public/image/event1.png";
import Title from "@/components/common/Title";
import smallPolygon from "public/image/landing/Polygon.png";
import yellowPolygon from "public/image/landing/Polygon-yellow.png";
import testIcon from "public/image/landing/vector.png";

import {
  NEWS_TEST_DEFAULT_LIST,
  TUTORIALS_TEST_DEFAULT_LIST,
} from "@/static/dummyData";

import {
  StBrandsContainer,
  StBrandsWrapper,
  StEventsContainer,
  StEventsWrapper,
  StHomeHero,
  StNewsListItem,
  StNewsListItemDetail,
  StNewsListWrapper,
  StNewsWrapper,
  StPageContainer,
  StTutorials,
  StTutorialsListItem,
  StTutorialsListWrapper,
} from "./style";

const HERO_DEFAULT_ITEMS = [
  { title: "NFT", image: logo1 },
  { title: "Crypto", image: logo2 },
  { title: "Meta", image: logo3 },
];
const Home = () => {
  const [selectedTap, setSelectedTap] = useState<string>("");
  const [current, setCurrent] = useState("");
  return (
    <StPageContainer>
      <div>charts bar</div>
      <StHomeHero>
        <button onClick={() => setSelectedTap("")}>تمامی محتوا</button>
        <div className="flex translate-y-[50%] transition">
          {HERO_DEFAULT_ITEMS.map(({ title, image }) => (
            <Hexagon
              $withChild
              key={title}
              isActive={selectedTap === title}
              onClick={() => setSelectedTap(title)}
            >
              <div className="flex h-full min-h-[130px] w-full flex-col items-center justify-between gap-3 text-center">
                <Image src={image} alt={title} width={85} height={85} />
                <h4>{title}</h4>
              </div>
            </Hexagon>
          ))}
        </div>
        <button onClick={() => setSelectedTap("")}> بلاگ</button>
      </StHomeHero>
      <Title>دوره‌ها و آموزش‌ها</Title>
      <StTutorials>
        <div className="absolute top-0 w-full max-w-[1300px] translate-y-[-50%] pr-20">
          <Link
            href="/tutorials"
            className=" rounded-3xl bg-yellow-400 px-5 py-2 text-sm "
          >
            مشاهده همه آموزش‌ها
          </Link>
        </div>
        <div className="absolute top-0 left-[3%] -translate-y-[50%]">
          <Image src={smallPolygon} alt="" width={80} height={80} />
        </div>
        <StTutorialsListWrapper>
          {TUTORIALS_TEST_DEFAULT_LIST.map(
            ({ img, title, detail, price, id }: any) => (
              <StTutorialsListItem key={id}>
                <Link href={`/tutorials/${id}`}>
                  <Image
                    src={img}
                    alt={title}
                    width={240}
                    height={200}
                    className="rounded-3xl"
                  />
                  <h5 className="my-2 font-bold">{title}</h5>
                  <p className="text-sm text-[#8A8A8A]">{detail}</p>
                  <div className="mt-3 flex items-center justify-between">
                    <span className="text-yellow-500">{price}</span>
                    <button className=" flex h-10 w-10 items-center justify-center rounded-full  bg-[#4430AB] ">
                      <FiChevronLeft color="white" />
                    </button>
                  </div>
                </Link>
              </StTutorialsListItem>
            )
          )}
        </StTutorialsListWrapper>
      </StTutorials>
      <Title>اخبار جدید </Title>
      <StNewsWrapper>
        <div className="absolute top-0 right-0 hidden translate-x-[50%] -translate-y-[50%] xl:block">
          <Image src={yellowPolygon} alt="" width={200} height={200} />
        </div>
        <StNewsListWrapper>
          {NEWS_TEST_DEFAULT_LIST.map(
            ({ title, creator, date, detail, id, img, tags }) => (
              <StNewsListItem key={id}>
                <Image src={img} alt={title} width={260} height={220} />
                <StNewsListItemDetail>
                  <h4 className="font-bold">{title}</h4>
                  <p className="text-sm">{detail}</p>
                  <div className="flex w-full justify-between">
                    <div className="flex items-center gap-5 text-xs text-gray-400">
                      <p>ساخته شده توسط {creator}</p>
                      <p>
                        در تاریخ &nbsp;
                        {Intl.DateTimeFormat("fa", {
                          year: "numeric",
                          month: "long",
                          day: "2-digit",
                        }).format(date)}
                      </p>
                      <ul className="flex gap-2">
                        {tags.map((i) => (
                          <li
                            key={i}
                            className=" rounded-2xl bg-violet-300 px-4 py-1 align-middle text-xs leading-6 text-purple-800"
                          >
                            {i}
                          </li>
                        ))}
                      </ul>
                    </div>
                    <Link
                      href={`/news/${id}`}
                      className=" rounded-3xl bg-violet-600 px-5 py-2 text-sm  text-white transition hover:bg-violet-500 "
                    >
                      مشاهده خبر
                    </Link>
                  </div>
                </StNewsListItemDetail>
              </StNewsListItem>
            )
          )}
        </StNewsListWrapper>
      </StNewsWrapper>
      <StEventsWrapper>
        <StEventsContainer>
          <div className="flex h-full w-full max-w-[240px] items-center">
            <div className="flex flex-col gap-10">
              <div className="flex flex-col gap-2">
                <span className="h-[6px] w-[20px] rounded-md bg-yellow-400" />
                <h4 className="text-lg font-bold text-white">
                  رویدادهای مهم پیش رو
                </h4>
                <p className="text-sm text-[#FBD50D]">
                  رویدادهای مهم عرصه ارز دیجیتال و NFT را می‌توانید از اینجا
                  دنبال کنید
                </p>
              </div>
              <div>
                <button className="w-full max-w-[190px] rounded-3xl bg-[#FBD50D] py-2 px-5 text-sm">
                  مشاهده همه رویدادها
                </button>
              </div>
            </div>
          </div>
          <div className="flex w-full max-w-[1000px]">
            <ul className="flex w-full justify-between gap-3 ">
              {Array.from({ length: 3 }).map((i, d) => (
                <li key={d}>
                  <Link
                    href={`/events/${d}`}
                    className="group flex cursor-pointer flex-col items-center justify-center rounded-3xl"
                  >
                    <Image src={event1} alt="" width={320} height={220} />
                    <div className="flex max-w-[260px] -translate-y-[30px] flex-col gap-3 rounded-3xl  bg-white py-4 px-6 transition group-hover:-translate-y-[10px] group-hover:shadow-2xl">
                      <h4 className="font-bold group-hover:text-[#4430AB]">
                        دوره آموزش NFT در بینوهاب
                      </h4>
                      <div className="flex flex-col text-xs leading-4 text-[#959595]">
                        <p className="border-b-2 py-2">
                          در این آمــوزش در رابطه با تنوع محصولات آن در این
                          پلتفرم NFT خواهیم پرداخت
                        </p>
                        <footer className="flex justify-between py-2">
                          <span>774 بازدید</span>
                          <span>16 اردیبهشت 1401</span>
                        </footer>
                      </div>
                    </div>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </StEventsContainer>
      </StEventsWrapper>
      <StBrandsWrapper>
        <StBrandsContainer>
          <div className="flex w-full flex-col gap-10">
            <Title rtl isPurple>
              برندها و اشخاص حوزه دیجیتال
            </Title>
            <div className="w-full max-w-[1300px]">
              <Swiper
                init
                className="px-3"
                autoplay
                spaceBetween={15}
                slidesPerView={4}
                pagination={{ clickable: true }}
                modules={[Pagination]}
                // onSlideChange={(active)=>s}
              >
                <div className="px-5">
                  {Array.from({ length: 6 }).map((i, d) => (
                    <SwiperSlide
                      key={Date.now() + "e" + d}
                      className="group relative flex cursor-pointer items-center justify-center rounded-3xl py-4 px-4 pl-0  hover:shadow-xl "
                    >
                      <div className="relative flex w-full gap-2">
                        <Image
                          src={testIcon}
                          alt=""
                          width={80}
                          height={80}
                          className="object-cover grayscale-[1] group-hover:grayscale-0"
                        />
                        <div className="flex flex-col justify-center">
                          <h5 className="text-lg font-bold">
                            لورم ایپسوم طراحان
                          </h5>
                          <p className="hidden text-sm text-gray-700 group-hover:block">
                            حوزه لورم ایپسوم طراحان گرافیک
                          </p>
                        </div>
                        <span className="absolute top-[50%] left-0  hidden h-[40px] w-[40px] -translate-x-[50%] -translate-y-[50%] items-center justify-center rounded-full bg-yellow-400 group-hover:flex ">
                          <FiArrowLeft />
                        </span>
                      </div>
                    </SwiperSlide>
                  ))}
                </div>
                <div className="pagination mt-10"></div>
              </Swiper>
            </div>
          </div>
        </StBrandsContainer>
      </StBrandsWrapper>
    </StPageContainer>
  );
};

export default Home;
