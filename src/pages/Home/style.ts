import tw from "tailwind-styled-components";

export const StPageContainer = tw.main`
flex 
flex-col
items-center
bg-slate-50
w-screen
min-h-screen
text-black
text-base
`;

export const StHomeHero = tw.div`
w-full
h-96
gap-2
relative
text-base
px-8 py-14
text-white
bg-hero_poster
bg-contain
bg-no-repeat
flex
items-end
justify-center
max-w-[1300px]
mb-24
`;

export const StTutorials = tw.section`
w-full
mb-8
relative
min-h-[500px]
flex
flex-col
justify-center
items-center
bg-white
bg-contain
bg-left
bg-no-repeat
text-black
`;

export const StTutorialsListWrapper = tw.ul`
flex
gap-2
items-center
justify-between
max-w-[1300px]
`;

export const StTutorialsListItem = tw.li`
p-3
cursor-pointer
bg-white
border-[1px]
border-gray-200
rounded-3xl
hover:shadow-2xl
hover:translate-y-[-10px]
transition
duration-500
`;

export const StNewsWrapper = tw.div`
w-full
flex 
mb-8
justify-center
min-h-[500px]
bg-white
relative
`;

export const StNewsListWrapper = tw.div`
flex
flex-col
gap-10
items-center
justify-between
max-w-[1300px]
`;

export const StNewsListItem = tw.div`
flex
gap-10
items-center
justify-between
max-w-[1300px]
`;

export const StNewsListItemDetail = tw.div`
flex
flex-col
py-2
gap-5
items-start
justify-between
max-w-[1300px]
`;
export const StEventsWrapper = tw.div`
flex
w-full
bg-right
bg-cover
relative
min-h-[500px]
justify-center
bg-bg_event
bg-no-repeat
`;

export const StEventsContainer = tw.div`
flex 
w-full
items-center
justify-between 
max-w-[1300px]
min-h-[500px]
`;

export const StBrandsWrapper = tw.div`
flex
w-full
justify-center
`;

export const StBrandsContainer = tw.div`
flex
w-full
py-14
justify-between
max-w-[1300px]
`;
