// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { TUTORIALS_TEST_DEFAULT_LIST } from "@/static/dummyData";
import type { NextApiRequest, NextApiResponse } from "next";
import { StaticImageData } from "next/image";

type Data = {
  name: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json({ name: "John Doe" });
}

interface NewsListProps {
  img: StaticImageData;
  title: string;
  detail: string;
  price: string;
  id: string;
}

export const getList = (
  req: NextApiRequest,
  res: NextApiResponse<NewsListProps[]>
) => {
  res.status(200).json(TUTORIALS_TEST_DEFAULT_LIST as NewsListProps[]);
};
