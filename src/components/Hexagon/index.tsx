import React from "react";
import ActiveHexagon from "public/image/Icons/HexIconActive.svg";
import NotActiveHexagon from "public/image/Icons/HexIconNotActive.svg";

import Image from "next/image";

const Hexagon = ({
  $withChild,
  isActive,
  children,
  className,
  onClick,
}: {
  $withChild?: boolean;
  className?: string;
  children?: any;
  isActive?: boolean;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
}) => {
  const containerClasses =
    " hex relative min-w-[270px] min-h-[270px] text-black group cursor-pointer ";
  return (
    <div
      className={className ? containerClasses + className : containerClasses}
      onClick={onClick}
    >
      <Image
        alt=""
        width={250}
        height={250}
        src={NotActiveHexagon}
        className="absolute top-[50%] left-[50%] z-10 translate-x-[-50%] translate-y-[-50%] drop-shadow-sm transition group-hover:translate-y-[-53%] group-hover:drop-shadow-xl"
      />
      {$withChild ? (
        <>
          <Image
            alt=""
            width={200}
            height={200}
            src={isActive ? ActiveHexagon : NotActiveHexagon}
            className="absolute top-[50%] left-[50%] z-10 translate-x-[-50%] translate-y-[-50%] drop-shadow-xl  transition group-hover:translate-y-[-53%]"
          />
        </>
      ) : (
        <></>
      )}
      <div
        className={`absolute top-[45%] left-[50%] z-20 translate-x-[-50%] translate-y-[-50%] transition group-hover:translate-y-[-53%] ${
          isActive ? "text-white" : "text-black"
        }`}
      >
        {children}
      </div>
    </div>
  );
};

export default Hexagon;
