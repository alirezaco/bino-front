import React, { FC } from "react";

interface TitleProps {
  children: string;
  rtl?: boolean;
  isPurple?: boolean;
}

const Title: FC<TitleProps> = ({
  children = "no title provided",
  rtl = false,
  isPurple = false,
}) => {
  return (
    <div
      className={`
      ${rtl ? "items-start" : "items-center"}
      flex flex-col justify-center gap-3 py-4 text-lg font-bold`}
    >
      <span
        className={`${
          isPurple ? "border-[#4430AB]" : "border-[#FBD50D]"
        } w-8 rounded-xl border-[4px] `}
      />
      {children}
    </div>
  );
};

export default Title;
