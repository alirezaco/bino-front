import { createPortal } from "react-dom";
import React, { FC, useEffect, useRef, useState } from "react";

interface ModalProps {
  children: any;
  onClose?: () => void;
  className?: string;
  isModalOpen?: boolean;
}

const Modal: FC<ModalProps> = ({
  children,
  onClose = () => {},
  className,
  isModalOpen = false,
}) => {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);

    return () => setMounted(false);
  }, []);

  const ref = useRef(null);

  if (mounted) {
    return isModalOpen
      ? createPortal(
          <div className="absolute top-0 left-0 z-10 h-screen w-screen">
            <span className="fixed top-0 left-0 h-full w-full bg-[#0008] backdrop-blur-sm" />
            <div
              ref={ref}
              className={`${
                className || ""
              } fixed top-[50%] left-[50%] z-20 -translate-x-[50%] -translate-y-[50%] h-screen w-screen flex items-center justify-center`}
              onClick={(e) => {
                if (e.target === ref.current) {
                  onClose();
                }
              }}
            >
              {children}
            </div>
          </div>,
          document.getElementById("portal")!
        )
      : null;
  } else {
    return children;
  }
};
export default Modal;
