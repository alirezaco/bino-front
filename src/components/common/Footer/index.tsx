"use client";
import React from "react";
import Link from "next/link";
import Image from "next/image";
import Logo from "public/image/colorlesslogo.png";
import {
  SiAparat,
  SiDiscord,
  SiLinkedin,
  SiReddit,
  SiTelegram,
  SiTwitter,
  SiYoutube,
} from "react-icons/si";
import { IoIosArrowUp } from "react-icons/io";
import {
  StFooterItems,
  StFooterWrapper,
  StFooterRights,
  StFooterRightsItem,
  StFooterSiteDetails,
} from "./style";
import { toast } from "react-toastify";

const Footer = () => {
  return (
    <StFooterWrapper>
      <StFooterItems>
        <div className="flex w-full justify-between gap-5 p-5">
          <StFooterSiteDetails>
            <Image src={Logo} alt="binoHub" width={160} height={85} />
            <p>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
              استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
              در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
              نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد
            </p>
          </StFooterSiteDetails>
          <div className="flex w-full justify-between">
            <div className="flex flex-col gap-3">
              {HEADER_DEFAULT_LINKS.map((i) => (
                <Link href={i.path} key={i.path}>
                  {i.title}
                </Link>
              ))}
            </div>

            <div className="flex flex-col gap-3">
              <h4>دریافت خبرنامه</h4>
              <div className="rounded-3xl bg-white p-2">
                <input
                  placeholder="ایمیل خود را وارد نمایید..."
                  className="bg-inherit pr-2"
                />
                <button
                  className="rounded-3xl bg-[#FBD50D] p-1 px-3 text-black"
                  onClick={() => toast.error("rr")}
                >
                  ثبت
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="flex w-full justify-between">
          <a
            href="#header"
            className="flex h-[40px] w-[40px] items-center justify-center rounded-full bg-[#FBD50D] text-xl"
          >
            <IoIosArrowUp color="#4430AB" />
          </a>
          <div className="flex gap-4">
            {EXTERNAL_LINKS.map(({ icon: IconFn, link, title }) => {
              const color = COLORS_CONFIG[title as string] || "black";
              const Icon = <IconFn color={color} />;
              return (
                <Link
                  href={link}
                  key={link}
                  passHref
                  className={`${title} flex items-center gap-2 rounded-3xl bg-white py-2 px-5`}
                >
                  {title} {Icon}
                </Link>
              );
            })}
          </div>
        </div>
      </StFooterItems>
      <StFooterRights>
        <StFooterRightsItem>
          <p>
            کلیه حقوق مادی و معنوی این سایت مربوط به شرکت بینوهاب می‌باشد.
            هرگونه کپی برداری با ذکر منبع بلامانع می‌باشد
          </p>
          <p>اسفند ماه 1401</p>
        </StFooterRightsItem>
      </StFooterRights>
    </StFooterWrapper>
  );
};

export default Footer;

const HEADER_DEFAULT_LINKS = [
  { title: "صفحه اصلی", path: "/" },
  { title: "اخبار", path: "/news" },
  { title: "آموزش", path: "/tutorials" },
  { title: "رویداد", path: "/events" },
  { title: "پادکست", path: "/podcasts" },
  { title: "خدمات", path: "/service" },
];

const EXTERNAL_LINKS = [
  { title: "telegram", icon: SiTelegram, link: "https://telegram.me" },
  { title: "aparat", icon: SiAparat, link: "https://aparat.ir" },
  { title: "youtube", icon: SiYoutube, link: "https://youtube.com" },
  { title: "reddit", icon: SiReddit, link: "https://reddit.com" },
  { title: "twitter", icon: SiTwitter, link: "https://twitter.com" },
  { title: "discord", icon: SiDiscord, link: "https://discord.com" },
  { title: "linkdin", icon: SiLinkedin, link: "https://linkdin.com" },
];

const COLORS_CONFIG: Record<string, string> = {
  telegram: "#29aaec",
  aparat: "#ED165A",
  youtube: "#FF0201",
  reddit: "#FF4500",
  twitter: "#55ACEF",
  discord: "#5865F2",
  linkdin: "#0077B7",
};
