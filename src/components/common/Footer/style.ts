import tw from "tailwind-styled-components";

export const StFooterWrapper = tw.div`
flex
flex-col
justify-center
items-center
bg-[#4F51DB]
relative
text-[#A1A1A1]
`;
export const StFooterItems = tw.div`
flex
p-10
text-white
flex-col
max-w-[1300px]
`;
export const StFooterSiteDetails = tw.div`
w-full
flex
flex-col
gap-10
`;

export const StFooterExternalLinks = tw.div`
flex
flex-col
max-w-[1300px]
`;

export const StFooterRights = tw.div`
w-full
h-[50px]
flex 
justify-center
bg-[#323232]
`;

export const StFooterRightsItem = tw.div`
w-full
flex
text-xs
justify-between
items-center
max-w-[1300px]
`;
