import React, { FC, ReactNode } from "react";

interface ButtonProps {
  children: string;
  colorString: string;
  className: string;
}

const Button: FC<ButtonProps> = ({ className, children, ...rest }) => {
  return (
    <button className={`rounded-3xl ` + className} {...rest}>
      {children}
      <span>icon</span>
    </button>
  );
};

export default Button;
