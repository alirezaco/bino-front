import Footer from "@/components/common/Footer";
import Header from "@/components/common/Header";
import "react-toastify/dist/ReactToastify.css";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <main className="flex min-h-[100vh] flex-col justify-between">
        <Header />
        {children}
        <Footer />
      </main>
    </>
  );
}
