import tw from "tailwind-styled-components";

export const StHeaderWrapper = tw.div`
flex
bg-white
items-center
justify-center
`;

export const StHeaderItems = tw.div`
flex
py-3
items-center
justify-between
max-w-[1300px]
w-full
`;

export const StLinksAndSearchWrapper = tw.div`
  flex
  gap-10
  text-sm
  items-center
`;
export const StLinksWrapper = tw.ul`
  flex
  gap-5
`;

export const StLinkItem = tw.div<{ $isActive: boolean }>`
${(p) => (p.$isActive ? "text-[#4430AB] font-bold" : "text-[#2F2F2F]")}
cursor-pointer
`;

export const StLoginAndSignUpAndCard = tw.div`
  flex
  gap-2
  items-center
`;
