"use client";
import Image from "next/image";
import React from "react";
import Logo from "public/image/photo_2023-02-02_22-00-10 copy 1.png";
import { NavLink } from "@/tools/components/NavLink";
import { RiSearch2Line, RiUserFill } from "react-icons/ri";
import { HiBellSnooze } from "react-icons/hi2";
import { BagIcon } from "public/image/Icons/bag";
import {
  StHeaderItems,
  StHeaderWrapper,
  StLinkItem,
  StLinksAndSearchWrapper,
  StLinksWrapper,
  StLoginAndSignUpAndCard,
} from "./style";

const Header = () => {
  return (
    <StHeaderWrapper id="header">
      <StHeaderItems>
        <Image
          src={Logo}
          alt="binoHub"
          width={160}
          height={65}
          className="ml-[2%]"
        />
        <StLinksAndSearchWrapper>
          <StLinksWrapper>
            {HEADER_DEFAULT_LINKS.map(({ title, path }) => (
              <NavLink
                exact
                href={path}
                key={path}
                childrenRenderer={(isActive) => (
                  <StLinkItem $isActive={isActive}>{title}</StLinkItem>
                )}
              />
            ))}
          </StLinksWrapper>
          <div className="flex items-center gap-6 text-xl">
            <RiSearch2Line />
            <span className=" flex h-10 w-10 items-center justify-center rounded-full  bg-[#FBD50D] ">
              <HiBellSnooze />
            </span>
          </div>
        </StLinksAndSearchWrapper>
        <StLoginAndSignUpAndCard>
          <button className="shadow-[0px_10px_20px_rgba(68,48,171,0.3) cursor-pointer] flex h-10 items-center gap-2 rounded-[30px] bg-[#4430AB] px-10 text-sm text-white">
            <RiUserFill />
            ورود/ ثبت نام
          </button>
          <span className=" flex h-10 w-10 items-center justify-center rounded-full  bg-[#D7D0F8]  ">
            <BagIcon />
          </span>
        </StLoginAndSignUpAndCard>
      </StHeaderItems>
    </StHeaderWrapper>
  );
};

export default Header;

const HEADER_DEFAULT_LINKS = [
  { title: "صفحه اصلی", path: "/" },
  { title: "اخبار", path: "/news" },
  { title: "آموزش", path: "/tutorials" },
  { title: "رویداد", path: "/events" },
  { title: "پادکست", path: "/podcasts" },
  { title: "خدمات", path: "/service" },
];
