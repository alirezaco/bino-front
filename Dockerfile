FROM node:18-alpine

COPY . .

RUN npm install

EXPOSE 3000

ENV PORT 3000
ENV NEXT_TELEMETRY_DISABLED 1

CMD ["npm", "run", "dev"]
